# nscdn: (Ab)using DNS as a CDN for chunked file transfer

```shell
touch store.sqlite3
docker run \
    -v `pwd`/store.sqlite3:/data/store.sqlite3 \
    --env NSCDN_ROOT=demo.example.com \
    leastfixedpoint/nscdn
```

```
docker run -d -v /root:/data -p 53:53 -p 53:53/udp --name nscdnd leastfixedpoint/nscdn
```

```
docker run -i --rm leastfixedpoint/nscdn nscdn get SEKIENAKASHITA.DEMO.NSCDN.ORG. | tee SekienAkashita.jpg | wc
```

## Licence

nscdn: (Ab)using DNS as a CDN for chunked file transfer  
Copyright © 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received [a copy of the GNU Affero General Public License](COPYING)
along with this program.  If not, see <https://www.gnu.org/licenses/>.
