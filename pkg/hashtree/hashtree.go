/// SPDX-License-Identifier: AGPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

package hashtree

import (
	"bytes"
	"encoding/base32"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"

	"golang.org/x/crypto/blake2b"
	"leastfixedpoint.com/nscdn/pkg/chunking/ronomon"
)

// Pointers:
//
// 64 bytes long.
//
// 0000000000000000111111111111111122222222222222223333333333333333
// 0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef
// ----------------------------------------------------------------
// ^^^^^^^^ type
//         ^^^^^^^^ length
//                 ^^^^^^^^^^^^^^^^ zero
//                                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ blake2b-256 hash
//
// Numeric quantities are stored big-endian.
//
// type = 1 --> hash points to raw data block
// type = 2 --> hash points to block containing more pointers
//
// No compression is applied: that's a transport thing.
//
// As DNS labels: length is implicit in the retrieved data, one way or another, so type and
// hash have to be encoded. string(Type) + "-" + base32(Hash)

type Hash [blake2b.Size256]byte

type BlobStore interface {
	SaveBlob(key Hash, value []byte) error
}

type BlobSource interface {
	LoadBlob(p Pointer) ([]byte, error)
}

type PointerType uint64

const (
	PointerTypeNone PointerType = iota
	PointerTypeData
	PointerTypeIndex
	PointerType_MAX
)

type Pointer struct {
	Type   PointerType
	Length uint64
	Hash   Hash
}

func (p *Pointer) Marshal() []byte {
	d := [64]byte{}
	binary.BigEndian.PutUint64(d[:8], uint64(p.Type))
	binary.BigEndian.PutUint64(d[8:16], p.Length)
	copy(d[32:64], p.Hash[:])
	return d[:]
}

func checkPointerType(t PointerType) error {
	if t == PointerTypeNone || t >= PointerType_MAX {
		return fmt.Errorf("Pointer type %d out of bounds", t)
	}
	return nil
}

func (p *Pointer) Unmarshal(d []byte) error {
	if len(d) != 64 {
		return fmt.Errorf("Cannot unmarshal Pointer: block has length %d, should be %d",
			len(d),
			64)
	}
	for _, z := range d[16:32] {
		if z != 0 {
			return errors.New("Nonzero byte found in reserved Pointer area")
		}
	}
	p.Type = PointerType(binary.BigEndian.Uint64(d[:8]))
	if err := checkPointerType(p.Type); err != nil {
		return err
	}
	p.Length = binary.BigEndian.Uint64(d[8:16])
	copy(p.Hash[:], d[32:64])
	return nil
}

var encoding *base32.Encoding = base32.StdEncoding.WithPadding(base32.NoPadding)

func (p *Pointer) Label() string {
	return fmt.Sprintf("%d-%s", p.Type, encoding.EncodeToString(p.Hash[:]))
}

func FromLabel(label string) (Pointer, error) {
	var p Pointer
	pieces := strings.SplitN(label, "-", 2)
	if len(pieces) != 2 {
		return p, errors.New("Bad label")
	}
	typeNum, err := strconv.ParseUint(pieces[0], 10, 64)
	if err != nil {
		return p, err
	}
	p.Type = PointerType(typeNum)
	if err := checkPointerType(p.Type); err != nil {
		return p, err
	}
	hash, err := encoding.DecodeString(pieces[1])
	if err != nil {
		return p, err
	}
	if len(hash) != blake2b.Size256 {
		return p, errors.New("Bad label hash")
	}
	copy(p.Hash[:], hash)
	return p, nil
}

func storeRaw(db BlobStore, body []byte) (Hash, error) {
	hash := blake2b.Sum256(body)
	if body == nil {
		buf := [0]byte{}
		return hash, db.SaveBlob(hash, buf[:])
	} else {
		return hash, db.SaveBlob(hash, body)
	}
}

func storeLeaf(db BlobStore, data []byte) (Pointer, error) {
	var p Pointer
	var err error
	p.Type = PointerTypeData
	p.Length = uint64(len(data))
	p.Hash, err = storeRaw(db, data)
	return p, err
}

func storeNode(db BlobStore, index []Pointer) (Pointer, error) {
	var p Pointer
	var data []byte
	var err error
	for _, q := range index {
		data = append(data, q.Marshal()...)
		p.Length += q.Length
	}
	p.Type = PointerTypeIndex
	p.Hash, err = storeRaw(db, data)
	return p, err
}

func storeTree(db BlobStore, index []Pointer) (Pointer, error) {
	// TODO: what about chunking the indices to avoid brittleness wrt insertions/deletions?
	var err error

	for len(index) > 512 /* 512 * 64 = 32k */ {
		var newindex []Pointer
		for i := 0; i < len(index); i += 512 {
			j := i + 512
			if j > len(index) {
				j = len(index)
			}
			var p Pointer
			p, err = storeNode(db, index[i:j])
			if err != nil {
				return Pointer{}, err
			}
			newindex = append(newindex, p)
		}
		index = newindex
	}

	if len(index) == 1 {
		return index[0], nil
	} else {
		return storeNode(db, index)
	}
}

func StoreData(db BlobStore, r io.Reader, p *ronomon.ChunkParameters) (Pointer, error) {
	var index []Pointer
	var err error

	for c := range ronomon.EnumerateChunks(r, p) {
		var p Pointer
		p, err = storeLeaf(db, c.Data)
		if err != nil {
			return Pointer{}, err
		}
		index = append(index, p)
	}

	return storeTree(db, index)
}

type Chunk struct {
	Data []byte
	Err  error
}

func StreamData(db BlobSource, p Pointer) chan Chunk {
	chunks := make(chan Chunk)
	go func() {
		defer close(chunks)
		err := streamData(chunks, db, p)
		if err != nil {
			chunks <- Chunk{Err: err}
		}
	}()
	return chunks
}

func streamData(chunks chan Chunk, db BlobSource, p Pointer) error {
	data, err := db.LoadBlob(p)
	if err != nil {
		return err
	}

	hash := blake2b.Sum256(data)
	if !bytes.Equal(hash[:], p.Hash[:]) {
		return fmt.Errorf("Corrupt chunk %s", p.Label())
	}

	switch p.Type {
	case PointerTypeData:
		chunks <- Chunk{Data: data}
		return nil
	case PointerTypeIndex:
		if len(data)%64 != 0 {
			return fmt.Errorf("Bad index length %d", len(data))
		}
		i := 0
		for i < len(data) {
			var q Pointer
			err = q.Unmarshal(data[i : i+64])
			if err != nil {
				return err
			}
			i += 64
			err = streamData(chunks, db, q)
			if err != nil {
				return err
			}
		}
		return nil
	default:
		return fmt.Errorf("Invalid pointer type %d", p.Type)
	}
}
