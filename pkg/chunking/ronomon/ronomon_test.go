/// SPDX-License-Identifier: AGPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

// Test cases after Nathan Fiedler's fastcdc-rs tests.
//
// The license for fastcdc-rs is:
//
//     The MIT License (MIT)
//
//     Copyright (c) 2020 Nathan Fiedler
//
//     Permission is hereby granted, free of charge, to any person obtaining a copy
//     of this software and associated documentation files (the "Software"), to deal
//     in the Software without restriction, including without limitation the rights
//     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//     copies of the Software, and to permit persons to whom the Software is
//     furnished to do so, subject to the following conditions:
//
//     The above copyright notice and this permission notice shall be included in all
//     copies or substantial portions of the Software.
//
//     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//     SOFTWARE.

package ronomon

import (
	"bytes"
	"io"
	"os"
	"testing"
)

func assertEq[V comparable](t *testing.T, actual, expected V) {
	if actual != expected {
		t.Errorf("expected %v; got %v", expected, actual)
	}
}

func TestLog2(t *testing.T) {
	assertEq(t, log2(0), 0)
	assertEq(t, log2(1), 0)
	assertEq(t, log2(2), 1)
	assertEq(t, log2(3), 2)
	assertEq(t, log2(5), 2)
	assertEq(t, log2(6), 3)
	assertEq(t, log2(11), 3)
	assertEq(t, log2(12), 4)
	assertEq(t, log2(19), 4)
	assertEq(t, log2(16383), 14)
	assertEq(t, log2(16384), 14)
	assertEq(t, log2(16385), 14)
	assertEq(t, log2(32767), 15)
	assertEq(t, log2(32768), 15)
	assertEq(t, log2(32769), 15)
	assertEq(t, log2(65535), 16)
	assertEq(t, log2(65536), 16)
	assertEq(t, log2(65537), 16)
	assertEq(t, log2(1048575), 20)
	assertEq(t, log2(1048576), 20)
	assertEq(t, log2(1048577), 20)
	assertEq(t, log2(4294967286), 32)
	assertEq(t, log2(4294967295), 32)
}

func TestCeilDiv(t *testing.T) {
	assertEq(t, ceilDiv(10, 5), 2)
	assertEq(t, ceilDiv(11, 5), 3)
	assertEq(t, ceilDiv(10, 3), 4)
	assertEq(t, ceilDiv(9, 3), 3)
	assertEq(t, ceilDiv(6, 2), 3)
	assertEq(t, ceilDiv(5, 2), 3)
}

func TestCenterSize(t *testing.T) {
	assertEq(t, centerSize(100, 50, 50), 0)
	assertEq(t, centerSize(100, 200, 50), 50)
	assertEq(t, centerSize(100, 200, 40), 40)
}

func TestAllZeros(t *testing.T) {
	array := [10240]byte{}
	chunks := Chunks(bytes.NewReader(array[:]), &ChunkParameters{64, 256, 1024})
	zerochunk := [1024]byte{}
	for i, c := range chunks {
		assertEq(t, c.Length, 1024)
		assertEq(t, c.Offset, uint64(i*1024))
		if !bytes.Equal(c.Data, zerochunk[:]) {
			t.Errorf("Chunk %d is incorrect", i)
		}
	}
}

func sekien() io.Reader {
	f, err := os.Open("../../../test/SekienAkashita.jpg")
	if err != nil {
		panic(err)
	}
	return f
}

func TestSekien16kChunks(t *testing.T) {
	results := Chunks(sekien(), &ChunkParameters{8192, 16384, 32768})
	assertEq(t, len(results), 6)
	assertEq(t, results[0].Offset, 0)
	assertEq(t, results[0].Length, 22366)
	assertEq(t, results[1].Offset, 22366)
	assertEq(t, results[1].Length, 8282)
	assertEq(t, results[2].Offset, 30648)
	assertEq(t, results[2].Length, 16303)
	assertEq(t, results[3].Offset, 46951)
	assertEq(t, results[3].Length, 18696)
	assertEq(t, results[4].Offset, 65647)
	assertEq(t, results[4].Length, 32768)
	assertEq(t, results[5].Offset, 98415)
	assertEq(t, results[5].Length, 11051)
}

func TestSekien32kChunks(t *testing.T) {
	results := Chunks(sekien(), &ChunkParameters{16384, 32768, 65536})
	assertEq(t, len(results), 3)
	assertEq(t, results[0].Offset, 0)
	assertEq(t, results[0].Length, 32857)
	assertEq(t, results[1].Offset, 32857)
	assertEq(t, results[1].Length, 16408)
	assertEq(t, results[2].Offset, 49265)
	assertEq(t, results[2].Length, 60201)
}

func TestSekien64kChunks(t *testing.T) {
	results := Chunks(sekien(), &ChunkParameters{32768, 65536, 131_072})
	assertEq(t, len(results), 2)
	assertEq(t, results[0].Offset, 0)
	assertEq(t, results[0].Length, 32857)
	assertEq(t, results[1].Offset, 32857)
	assertEq(t, results[1].Length, 76609)
}
