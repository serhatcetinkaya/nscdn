/// SPDX-License-Identifier: AGPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"os"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/mattn/go-sqlite3"
	_ "github.com/mattn/go-sqlite3"
	"leastfixedpoint.com/nscdn/pkg/chunking/ronomon"
	"leastfixedpoint.com/nscdn/pkg/hashtree"
	"leastfixedpoint.com/nscdn/pkg/nscdn"
)

var (
	logLevel = log.WarnLevel

	addCmd = flag.NewFlagSet("add", flag.ExitOnError)

	delCmd = flag.NewFlagSet("del", flag.ExitOnError)

	initCmd = flag.NewFlagSet("init", flag.ExitOnError)

	getCmd    = flag.NewFlagSet("get", flag.ExitOnError)
	getDomain = getCmd.String("domain", "", "NSCDN server domain (optional)")
	getHost   = getCmd.String("host", "", "DNS server to query (defaults to the system's nameservers)")
	getPort   = getCmd.Int("port", 53, "Port to use for DNS queries")
)

type BlobStore struct {
	db       *sql.DB
	inserter *sql.Stmt
}

func NewBlobStore(db *sql.DB) (*BlobStore, error) {
	var b BlobStore
	var err error
	b.db = db
	b.inserter, err = db.Prepare("insert into chunks values(?, ?) on conflict do nothing")
	if err != nil {
		return nil, err
	}
	return &b, nil
}

func (b *BlobStore) SaveBlob(key hashtree.Hash, value []byte) error {
	log.Debugf("saving %d bytes at %x", len(value), key)
	_, err := b.inserter.Exec(key[:], value)
	return err
}

func usageError(f *flag.FlagSet, msg string) {
	fmt.Fprintf(os.Stderr, "ERROR: %s\n\n", msg)
	f.Usage()
	os.Exit(1)
}

func setupDb(storefile string) *BlobStore {
	db, err := sql.Open("sqlite3", storefile)
	if err != nil {
		log.Fatal(err)
	}
	db.Exec("create table files(name text not null primary key, pointer blob not null)")
	db.Exec("create table chunks(hash blob not null primary key, body blob not null)")

	store, err := NewBlobStore(db)
	if err != nil {
		log.Fatal(err)
	}

	for {
		_, err = db.Exec("begin immediate")
		if err == nil {
			break
		}
		if err == sqlite3.ErrBusy {
			log.Info("Waiting for other writers to finish")
		} else {
			log.Fatal(err)
		}
	}

	return store
}

func main() {
	flag.Func("log", "Set log level (trace, debug, info, warn, error, fatal, panic)", func(s string) error {
		level, err := log.ParseLevel(s)
		if err == nil {
			logLevel = level
		}
		return err
	})

	cmd := path.Base(os.Args[0])
	addCmd.Usage = func() {
		fmt.Printf("%s [GLOBAL OPTIONS] add [OPTIONS] <storefile> <label>\n", cmd)
		fmt.Println("  Adds data from stdin to the given storefile, named by the given label.")
		fmt.Println("  Labels are case-insensitive.")
		fmt.Println()
		addCmd.PrintDefaults()
	}
	delCmd.Usage = func() {
		fmt.Printf("%s [GLOBAL OPTIONS] del [OPTIONS] <storefile> <label>\n", cmd)
		fmt.Println("  Removes the given label from the given storefile. Idempotent.")
		fmt.Println("  Labels are case-insensitive.")
		fmt.Println()
		delCmd.PrintDefaults()
	}
	initCmd.Usage = func() {
		fmt.Printf("%s [GLOBAL OPTIONS] init [OPTIONS] <storefile>\n", cmd)
		fmt.Println("  Initializes the given storefile. Idempotent.")
		fmt.Println()
		initCmd.PrintDefaults()
	}
	getCmd.Usage = func() {
		fmt.Printf("%s [GLOBAL OPTIONS] get [OPTIONS] <name>\n", cmd)
		fmt.Println("  Retrieves a value from a server, writing content to stdout.")
		fmt.Println()
		fmt.Println("  If `-domain` is given and `name` does not end in \".\",")
		fmt.Println("  acts as if `name` had the value of `-domain` appended to it.")
		fmt.Println("  Otherwise uses `name` as-is.")
		fmt.Println("  Names are case-insensitive.")
		fmt.Println()
		getCmd.PrintDefaults()
	}

	flag.Usage = func() {
		fmt.Println("---------------------------------------------------------------------------")
		fmt.Printf("Usage: %s [OPTIONS] COMMAND\n", cmd)
		fmt.Println()
		fmt.Println("GLOBAL OPTIONS")
		fmt.Println()
		flag.PrintDefaults()
		fmt.Println()
		fmt.Println("---------------------------------------------------------------------------")
		fmt.Println("STORE MANAGEMENT COMMANDS (local)")
		fmt.Println()
		fmt.Println("  These commands manipulate a local sqlite3 file.")
		fmt.Println()
		addCmd.Usage()
		fmt.Println()
		delCmd.Usage()
		fmt.Println()
		initCmd.Usage()
		fmt.Println()
		fmt.Println("---------------------------------------------------------------------------")
		fmt.Println("PROTOCOL CLIENT COMMANDS (network)")
		fmt.Println()
		fmt.Println("  These commands access a server across the network.")
		fmt.Println()
		getCmd.Usage()
		fmt.Println()
	}
	flag.Parse()

	log.SetLevel(logLevel)

	if len(flag.Args()) < 1 {
		usageError(flag.CommandLine, "Missing subcommand name")
	}

	var store *BlobStore

	switch flag.Arg(0) {
	case "add":
		addCmd.Parse(flag.Args()[1:])
		if len(addCmd.Args()) < 2 {
			usageError(addCmd, "Missing storefile and/or label")
		}
		store = setupDb(addCmd.Arg(0))
		label := strings.ToUpper(addCmd.Arg(1))

		pointer, err := hashtree.StoreData(store, os.Stdin, &ronomon.ChunkParameters{
			LowerLimit:  8192,
			AverageSize: 16384,
			UpperLimit:  49152,
		})
		if err != nil {
			log.Fatal(err)
		}
		log.WithFields(log.Fields{
			"filename": label,
			"label":    pointer.Label(),
		}).Info("Top hash")
		if _, err := store.db.Exec(
			"insert into files values (?, ?) on conflict(name) do update set pointer=excluded.pointer",
			label,
			pointer.Marshal()); err != nil {
			log.Fatal(err)
		}
	case "del":
		delCmd.Parse(flag.Args()[1:])
		if len(delCmd.Args()) < 2 {
			usageError(delCmd, "Missing storefile and/or label")
		}
		store = setupDb(delCmd.Arg(0))
		label := strings.ToUpper(delCmd.Arg(1))
		store.db.Exec("delete from files where name = ?", label)
	case "init":
		initCmd.Parse(flag.Args()[1:])
		if len(initCmd.Args()) < 1 {
			usageError(initCmd, "Missing storefile")
		}
		setupDb(initCmd.Arg(0))
	case "get":
		getCmd.Parse(flag.Args()[1:])
		if len(getCmd.Args()) < 1 {
			usageError(getCmd, "Missing name")
		}
		api := &nscdn.Api{
			Domain: *getDomain,
			Host:   *getHost,
			Port:   int16(*getPort),
		}
		for c := range api.Get(getCmd.Arg(0)) {
			if c.Err != nil {
				log.Fatal(c.Err)
			}
			_, err := os.Stdout.Write(c.Data)
			if err != nil {
				log.Fatal(err)
			}
		}
	default:
		usageError(flag.CommandLine, "Unknown subcommand")
	}

	if store != nil {
		if _, err := store.db.Exec("commit"); err != nil {
			log.Fatal(err)
		}
	}
}
