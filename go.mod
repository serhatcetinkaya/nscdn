module leastfixedpoint.com/nscdn

go 1.20

require github.com/miekg/dns v1.1.55

require github.com/stretchr/testify v1.8.1 // indirect

require (
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/crypto v0.11.0
	golang.org/x/mod v0.7.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	golang.org/x/tools v0.3.0 // indirect
)
